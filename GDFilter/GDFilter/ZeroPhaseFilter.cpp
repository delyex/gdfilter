﻿#include "stdafx.h"

CZeroPhaseFilter::CZeroPhaseFilter(unsigned int uiOrder) :CGDFilter(uiOrder)
{

}

CZeroPhaseFilter::~CZeroPhaseFilter(void)
{
	free(m_dXExpanded);
	free(m_dZ);
	free(m_dYExpanded);
}


double * CZeroPhaseFilter::Do(double dInputs[], unsigned int uiLen)
{
	m_uiDataLen = uiLen;
	m_dXArray = dInputs;
	m_uiMargin = 3 * m_uiOrder;
	ExpandXArray(m_uiMargin);
	unsigned int uiTotalLen = m_uiDataLen + 2 * m_uiMargin;
	double *dYForeward = (double*)calloc(uiTotalLen, sizeof(double));
	m_dYExpanded = (double*)calloc(uiTotalLen, sizeof(double));

	double *ypast = (double*)calloc(m_uiOrder, sizeof(double));
	double *xpast = (double*)calloc(m_uiOrder, sizeof(double));
	double *zi = (double*)calloc(m_uiOrder, sizeof(double));
	for (unsigned int i = 0; i < m_uiOrder; i++)
	{
		ypast[i] = xpast[i] = 1.0; // 输入输出均为1，得到的m_dZ相当于系数
	}

	m_dZ = FiltInitCond(m_uiOrder, m_dNum, m_dDen, ypast, xpast);

	// 正向
	for (unsigned int i = 0; i < m_uiOrder; i++)
	{
		zi[i] = m_dZ[i] * m_dXExpanded[0];
	}
	for (unsigned int i = 0; i < uiTotalLen; i++)
	{
		dYForeward[i] = CGDFilter::Do(m_dXExpanded[i], zi);
	}

	// 反向
	for (unsigned int i = 0; i < m_uiOrder; i++)
	{
		zi[i] = m_dZ[i] * dYForeward[uiTotalLen - 1];
	}

	Restart();
	for (int i = (uiTotalLen - 1); i >= 0; i--)
	{
		m_dYExpanded[i] = CGDFilter::Do(dYForeward[i], zi);
	}

	m_dYArray = m_dYExpanded + m_uiMargin;

	free(dYForeward);

	return m_dYArray;
}

//************************************
// Method:    ExpandXArray 对输入数据两端进行扩展，各扩展出uiMargin的长度
// FullName:  ZeroPhaseFilter::ExpandXArray
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: unsigned int uiMargin 边缘扩展长度
//************************************
void CZeroPhaseFilter::ExpandXArray(unsigned int uiMargin)
{
	m_uiMargin = uiMargin;
	unsigned int uiTotalLen = m_uiDataLen + 2 * m_uiMargin;
	m_dXExpanded = (double*)calloc(uiTotalLen, sizeof(double));
	unsigned int i;

	for (i = 0; i < m_uiMargin; i++)
	{
		m_dXExpanded[i] = 2.0 * m_dXArray[0] - m_dXArray[m_uiMargin - i];
		m_dXExpanded[uiTotalLen - 1 - i] = 2.0 * m_dXArray[m_uiDataLen - 1] - m_dXArray[m_uiDataLen - 1 - m_uiMargin + i];
	}
	for (i = 0; i < m_uiDataLen; i++)
	{
		m_dXExpanded[m_uiMargin + i] = m_dXArray[i];
	}
}

