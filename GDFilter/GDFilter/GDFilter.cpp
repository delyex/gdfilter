﻿#include "stdafx.h"


//************************************
// Method:    GDFilter
// FullName:  GDFilter::GDFilter
// Access:    private 
// Returns:   
// Qualifier:
// Parameter: unsigned int uiOrder 滤波器阶数
//************************************
CGDFilter::CGDFilter(unsigned int uiOrder)
{
	m_uiOrder = uiOrder;
	m_dNum = (double*)calloc(m_uiOrder + 1, sizeof(double));
	m_dDen = (double*)calloc(m_uiOrder + 1, sizeof(double));
	m_dXz = (double*)calloc(m_uiOrder + 1, sizeof(double));
	m_dYz = (double*)calloc(m_uiOrder + 1, sizeof(double));
	m_dDen[0] = 1.0; // 初始化滤波器分母，构造 FIR 滤波器的话可以不初始化分母；
	// TODO: 分子分母根据传入分子分母维数不同，申请不同大小的内存，以节省内存空间
	m_uiICCnt = 0;
}
CGDFilter::~CGDFilter(void)
{
	free(m_dNum);
	free(m_dDen);
	free(m_dXz);
	free(m_dYz);
}

//************************************
// Method:    SetNum 设置滤波器分子系数
// FullName:  GDFilter::SetNum
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: double dNum 
//************************************
void CGDFilter::SetNum(double dNum, ...)
{
	for (unsigned int i = 0; i < (m_uiOrder + 1); i++)
	{
		*(m_dNum + i) = *(&dNum + i);
	}
}

void CGDFilter::SetNum(double dNum[])
{
	for (unsigned int i = 0; i < (m_uiOrder + 1); i++)
	{
		*(m_dNum + i) = *(dNum + i);
	}
}

//************************************
// Method:    SetDen 设置滤波器分母系数
// FullName:  GDFilter::SetDen
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: double dDen
//************************************
void CGDFilter::SetDen(double dDen, ...)
{
	for (unsigned int i = 0; i < (m_uiOrder + 1); i++)
	{
		*(m_dDen + i) = *(&dDen + i);
	}
}

void CGDFilter::SetDen(double dDen[])
{
	for (unsigned int i = 0; i < (m_uiOrder + 1); i++)
	{
		*(m_dDen + i) = *(dDen + i);
	}
}

double CGDFilter::Do(double dInput)
{
	m_dXz[0] = dInput;
	m_dYz[0] = m_dXz[0] * m_dNum[0];
	for (int i = m_uiOrder; i > 0; i--)
	{
		m_dYz[0] += m_dNum[i] * m_dXz[i] - m_dDen[i] * m_dYz[i];
		m_dXz[i] = m_dXz[i - 1];
		m_dYz[i] = m_dYz[i - 1];
	}

	return m_dYz[0];
}

//************************************
// Method:    FiltInitCond 计算滤波器的初始条件
// FullName:  GDFilter::FiltInitCond
// Access:    private 
// Returns:   void
// Qualifier:
// Parameter: unsigned int uiOrder
// Parameter: double b[]
// Parameter: double a[]
// Parameter: double ypast[] The vectors xpast and ypast contain the most recent input or output first, and oldest input or output last.
// Parameter: double xpast[] The vectors xpast and ypast contain the most recent input or output first, and oldest input or output last.
//************************************
double * CGDFilter::FiltInitCond(unsigned int uiOrder, double Num[], double Den[], double ypast[], double xpast[])
{
	double *zi = (double*)calloc(uiOrder, sizeof(double));
	double *b = (double*)calloc(uiOrder, sizeof(double));
	double *a = (double*)calloc(uiOrder, sizeof(double));
	double *vinit = (double*)calloc(uiOrder, sizeof(double));
	double *vx = (double*)calloc(uiOrder, sizeof(double));

	unsigned int i;

	for (i = 0; i < uiOrder; i++)
	{
		a[i] = -Den[uiOrder - i] / Den[0];
		b[i] = Num[uiOrder - i] / Den[0];
	}

	CGDFilter filter1 = CGDFilter(uiOrder - 1);
	CGDFilter filter2 = CGDFilter(uiOrder - 1);

	filter1.SetNum(a);
	filter2.SetNum(b);

	for (i = 0; i < uiOrder; i++)
	{
		vinit[i] = filter1.Do(ypast[i]);
		vx[i] = filter2.Do(xpast[i]);
		zi[uiOrder - 1 - i] = vinit[i] + vx[i];
	}

	free(b);
	free(a);
	free(vinit);
	free(vx);
	return zi;
}

//************************************
// Method:    Do 包含初始条件呃滤波器计算
// FullName:  GDFilter::Do
// Access:    public 
// Returns:   double
// Qualifier:
// Parameter: double dInput
// Parameter: double dZ[] dZ的长度为滤波器维数 m_uiOrder
//************************************
double CGDFilter::Do(double dInput, double dZ[])
{
	m_dXz[0] = dInput;
	m_dYz[0] = m_dXz[0] * m_dNum[0];
	for (int i = m_uiOrder; i > 0; i--)
	{
		m_dYz[0] += m_dNum[i] * m_dXz[i] - m_dDen[i] * m_dYz[i];
		m_dXz[i] = m_dXz[i - 1];
		m_dYz[i] = m_dYz[i - 1];
	}

	if (m_uiICCnt < m_uiOrder)
	{
		m_dYz[0] += dZ[m_uiICCnt];
		m_dYz[1] = m_dYz[0];
		m_uiICCnt++;
	}

	return m_dYz[0];
}


//************************************
// Method:    Restart 重启滤波器
// FullName:  GDFilter::Restart
// Access:    public 
// Returns:   void
// Qualifier:
// Parameter: void
//************************************
void CGDFilter::Restart(void)
{
	m_uiICCnt = 0;
	memset(m_dXz, 0, (m_uiOrder + 1) * sizeof(double));
	memset(m_dYz, 0, (m_uiOrder + 1) * sizeof(double));
}
