﻿#pragma once
class CGDFilter
{
protected:
	unsigned int m_uiOrder; // 滤波器阶数
	double *m_dNum; // 分子
	double *m_dDen; // 分母
	double *m_dXz; // 输入，数组下标越大，数据越“旧”，即 m_dXz[n] = (z^-n)X
	double *m_dYz; // 输出，数组下标越大，数据越“旧”，即 m_dYz[n] = (z^-n)Y
	unsigned int m_uiICCnt; // Initial condition count;
public:
	CGDFilter(unsigned int uiOrder);
	~CGDFilter(void);
	double Do(double dInput);
	double * FiltInitCond(unsigned int uiOrder, double Num[], double Den[], double ypast[], double xpast[]);
	double Do(double dInput, double dZ[]);
	void Restart(void);
	void SetNum(double dNum, ...);
	void SetNum(double dNum[]);
	void SetDen(double dDen, ...);
	void SetDen(double dDen[]);
};

