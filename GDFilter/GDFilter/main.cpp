﻿// GDFilter.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

int main()
{
	FILE *fRead, *fWrite;
	fopen_s(&fRead, "data.dat", "r");
	fopen_s(&fWrite, "data_filtered.dat", "w");

	// 1. 构造实例：12阶滤波器
	CZeroPhaseFilter filter = CZeroPhaseFilter(12);
	
	// 2. 设定滤波器参数：12阶巴特沃斯低通滤波器，fs=2Hz，fc=0.15Hz
	filter.SetNum(5.89610018737087e-09, 7.07532022484504e-08, 3.89142612366477e-07, 1.29714204122159e-06, 2.91856959274858e-06, 4.66971134839773e-06, 5.44799657313068e-06,
		4.66971134839773e-06, 2.91856959274858e-06, 1.29714204122159e-06, 3.89142612366477e-07, 7.07532022484504e-08, 5.89610018737087e-09);
	filter.SetDen(1, -8.39117936683878, 32.6799573924969, -78.0154951368927, 127.021368301706, -148.470966294543, 127.660549277552,
		-81.3085256256591, 38.0511444623558, -12.7543178430412, 2.90528307566679, -0.403654776265225, 0.0258606838884198);

	// 3. 设定滤波器输入
	unsigned int uiLines = 0;
	while (!feof(fRead))
	{
		fscanf_s(fRead, "%*[^\n]%*c");
		uiLines++;
	}
	uiLines -= 1; // 去掉最后一行（最后一行有可能只有回车）

	fseek(fRead, 0, SEEK_SET);

	double *dInputs = (double*)calloc(uiLines, sizeof(double));
	for (unsigned int i = 0; i < uiLines; i++)
	{
		fscanf_s(fRead, "%lf", dInputs + i);
	}

	// 4. 滤波计算
	double *dOutputs = filter.Do(dInputs, uiLines);

	// 5. 保存滤波结果
	for (unsigned int i = 0; i < uiLines; i++)
	{
		fprintf_s(fWrite, "%lf\r\n", dOutputs[i]);
	}

	// 6. 清理内存
	free(dInputs);

	fclose(fRead);
	fclose(fWrite);
	return 0;
}

