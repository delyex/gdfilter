﻿class CZeroPhaseFilter : public CGDFilter
{
private:
	unsigned int m_uiDataLen; // 数据长度
	double *m_dXArray; // 实际的滤波器输入
	double *m_dXExpanded; // 计算用的滤波器输入（包含扩展值）
	double *m_dYExpanded; // 计算用的滤波器输出（包含扩展值）
	unsigned int m_uiMargin;
	double *m_dZ;
public:
	double *m_dYArray; // 实际的滤波器输出（不包含扩展）
public:
	CZeroPhaseFilter(unsigned int uiOrder);
	~CZeroPhaseFilter(void);
	double * Do(double dInputs[], unsigned int uiLen);
private:
	void ExpandXArray(unsigned int uiMargin);
};
