# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* 包含一个通用滤波器类 CGDFilter;
* 包含一个零相位滤波器 CZeroPhaseFilter;

### How do I get set up? ###

1. 构造实例；
2. 设定滤波器参数；
3. 设定滤波器输入；
4. 滤波计算；
5. 保存滤波结果。

参见 main.cpp 实例。

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### Reference ###
[Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)